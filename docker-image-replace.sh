#!/bin/bash

# Script used to replace Docker images

if [[ -z "$2" ]];then
    echo "Usage:"
    echo "$0 <origin:tag> <destination:new_tag>"
    echo ""
    echo "e.g. $0 quickstart/testing:3.2 quickstart:3.2"
    exit 1
fi

orig="$1"
dest="$2"

if [[ "$orig" =~ ":" || "$dest" =~ ":" ]]
then
    sudo docker pull "registry.gitlab.com/zatosource/docker-registry/$orig" && \
    sudo docker tag "registry.gitlab.com/zatosource/docker-registry/$orig" "registry.gitlab.com/zatosource/docker-registry/$dest" && \
    sudo docker push "registry.gitlab.com/zatosource/docker-registry/$dest"
else
    echo "At least one of the Docker images specified does not includes the tag"
    exit 1
fi
